<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
        parent::__construct();
        $this->load->model('web_Model','web');
        
    }
	public function index()
	{
		$this->load->view('include/header');
		$this->load->view('index');
		$this->load->view('include/footer');
	}
	public function view_products(){

    $where = array('quantityinStock!='=>'0');
    $data['products'] = $this->web->doGetalldata($where,'products');

    $this->load->view('include/header');
    $this->load->view('view_products',$data);
    $this->load->view('include/footer');
  }
         private function error($data)
    {
        die(json_encode(array("status"=>"error","data"=>$data)));
    }
     private function success($data)
    {
        die(json_encode(array("status"=>"success","data"=>$data)));
    }
}
