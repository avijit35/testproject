<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
        parent::__construct();
        $this->load->model('web_Model','web');
        
    }
	public function orders($order_id){

$where = array('orderNumber='=>$order_id);
$order = $this->web->doGetdata($where,'orders');

 if(!empty($order)){
       
       $obj = new Stdclass();
       $obj->order_id = $order->orderNumber;
       $obj->order_date = $order->orderDate;
       $obj->status = $order->status;
        
       $where = array('customerNumber='=>$order->customerNumber);
       $customer = $this->web->doGetdata($where,'customers');
         $obj1 = new Stdclass();
         $obj1->first_name = $customer->contactFirstName;
         $obj1->last_name = $customer->contactLastName;
         $obj1->phone = $customer->phone;
         $obj1->countrycode = $customer->country;
      $obj->customer = $obj1;
         $where = array('orderNumber='=>$order->orderNumber);
         $orders = $this->web->doGetalldata($where,'orderdetails');

$total  = 0;
          $array = array();
         foreach ($orders as $key => $value) {
 
             $where = array('productCode='=>$value->productCode);
             $product = $this->web->doGetdata($where,'products');
               $obj2 = new Stdclass();
               $obj2->product = $product->productName;
               $obj2->productline = $product->productLine;
               $obj2->unit_price = $value->priceEach;
               $obj2->qty = $value->quantityOrdered;
               $obj2->linetotal = round(($value->priceEach * $value->quantityOrdered),2);
               $array[] = $obj2;
             $total = $total  + round(($value->priceEach * $value->quantityOrdered),2);         

         }

         $array['bill_amount'] = number_format($total,2);
    $obj->order_details = $array;
    $this->success($obj);

 }else{
  // $array = array("status"=>false,"no"=>400,"message"=>"error");
  //         die(json_encode($array));
  // show_error('<p style="font-size:20px"> error</p>', 'Invalid Order Id.', '<b></b>', 400);
  var_dump(http_response_code(400));

 }


  }
         private function error($data)
    {
        die(json_encode(array("status"=>"error","data"=>$data)));
    }
     private function success($data)
    {
        die(json_encode(array("status"=>"success","data"=>$data)));
    }
}
