<!-- Footer Section Begin -->
    <footer class="footer-section">
        
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All Rights Reserved </a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                        <
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="<?php echo base_url('assets/web/');?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/jquery.countdown.min.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/jquery.zoom.min.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/jquery.dd.min.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/jquery.slicknav.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/web/');?>js/main.js"></script>
</body>

</html>