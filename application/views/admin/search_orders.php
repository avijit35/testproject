
<body>

<div id="container">
	<!-- <center><h1>Welcome Admin!</h1></center>
   <a href="<?php echo base_url('admin/search_orders');?>">Search Orders</a> -->
	
        <div class="row">
        	<div class="col-md-6">
        		<!-- <label></label> -->
             <input type="text" id='order_id' placeholder="Search by order by" class="form-control">
        	</div>
             <button onclick="search()" class="btn btn-success">Search</button>
        </div>
        <h4 id="order_details"><h4>
        
           
	<div id="body">
		<!-- <h1>DEMO PROJECT.</h1> -->

		<!-- <p>If you would like to edit this page you'll find it located at:</p>
		<code>application/views/welcome_message.php</code>

		<p>The corresponding controller for this page is found at:</p>
		<code>application/controllers/Welcome.php</code>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p> -->
	</div>

	<!-- <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p> -->
</div>

</body>
<script>
	
function search(){
	var order_id = $("#order_id").val();
	if(order_id==''){
		swal('Please Enter order id','','error');
		return false;
	}

	$.ajax({
		url:'<?php echo base_url('api/orders/');?>'+order_id,
		type:"GET",
		success:function(response){
			json=JSON.parse(response);
			if(json.status=='success'){
                   var data = json.data;
                   console.log(data);
              
                  var str = 'Order Id: '+data.order_id+',  Order date: '+data.order_date+',  Order Status: '+data.status+', Bill amount: '+data.order_details.bill_amount+'';
                 $("#order_details").html(str);
			}else{
                  var str = 'Invalid Order id';
                  $("#order_details").html(str);
			}
		}

	})
}

</script>
