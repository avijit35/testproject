<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Lottery ">
    <meta name="keywords" content="Lottery ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Project </title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/web/');?>css/style.css" type="text/css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
  
        <div class="container">
            <div class="inner-header">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="logo">
                            <a href="<?php echo base_url();?>">
                                <img src="" alt="" width="150px">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8">
                     <div class="links">

					 </div>
                    </div>
       
                </div>
            </div>
        </div>
		<div class="main-menu">
<nav class="navbar navbar-expand-md  navbar-dark">
<div class="container">
  <!-- Brand -->
  
  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav mx-auto">
      <li class="nav-item">
        <a class="nav-link active" href="<?php echo base_url();?>">Home</a>
      </li>
      
    </ul>
  </div>
</nav>
</div>
</div>
    </header>
    <!-- Header End -->